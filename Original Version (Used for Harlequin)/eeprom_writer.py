# Based partly on Ben Eaters https://github.com/beneater/eeprom-programmer/blob/master/eeprom-programmer/eeprom-programmer.ino

import wiringpi
import time
import numpy as np
import math

SHIFT_DATA = 0          # SER   14
SHIFT_CLK = 1           # SRCLK 11
SHIFT_LATCH = 2         # RCLK  12
WRITE_EN = 4

EEPROM_DATA = [29, 28, 27, 25, 24, 23, 22, 21]

INPUT = 0
OUTPUT = 1
MSBFIRST = 1
LOW = 0
HIGH = 1

EEPROM_BYTES = 524288
BANK_SIZE = 65536

COMMAND_TIME = 0.0001 # 100 µs should suffice, page 29 datasheet


def shiftOut(shift_data, shift_clk, data):
    for i in range(7,-1,-1):
        wiringpi.digitalWrite(shift_data, HIGH if data & (1 << i) != 0 else LOW)
        wiringpi.digitalWrite(shift_clk, HIGH)
        wiringpi.digitalWrite(shift_clk, LOW)


def setAddress(address, outputEnable):

    # set pin 20 HIGH if outputEnable = false
    shiftOut(SHIFT_DATA, SHIFT_CLK, ((address >> 16) | (0x00 if outputEnable else 0x08)))
    shiftOut(SHIFT_DATA, SHIFT_CLK, (address >> 8))
    shiftOut(SHIFT_DATA, SHIFT_CLK, (address))

    wiringpi.digitalWrite(SHIFT_LATCH, LOW)
    wiringpi.digitalWrite(SHIFT_LATCH, HIGH)
    wiringpi.digitalWrite(SHIFT_LATCH, LOW)


def setData(data):
    for pin in EEPROM_DATA:
        wiringpi.digitalWrite(pin, int(data & 1))
        data = data >> 1


def readEEPROM(address):
    setAddress(address, True)
    data = 0
    for pin in reversed(EEPROM_DATA):
        data = (data << 1) + wiringpi.digitalRead(pin)
    return data


def writeAM29F040(address, data):
    setAddress(0x555, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False)
    setData(0xA0)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(address, False)
    setData(data)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)


def eraseAM29F040():
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, OUTPUT)

    setAddress(0x555, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False)
    setData(0x80)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False)
    setData(0x10)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)


#def writeEEPROM(address, data):
#    setAddress(address, False)
#    for pin in EEPROM_DATA:
#        wiringpi.pinMode(pin, OUTPUT)
#    for pin in EEPROM_DATA:
#        wiringpi.digitalWrite(pin, int(data & 1))
#        data = data >> 1

#    wiringpi.digitalWrite(WRITE_EN, LOW)
#    time.sleep(0.0000001)
#    wiringpi.digitalWrite(WRITE_EN, HIGH)
#    time.sleep(0.01)


def writeEEPROMFile(path):
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, INPUT)
    data_arr = [0xff] * EEPROM_BYTES
    for i in range(EEPROM_BYTES):
        data_arr[i] = readEEPROM(i)

    byte_arr = bytearray(data_arr)
    with open(path, "wb") as f:
        f.write(byte_arr)


def readROM(path):
    with open(path,"rb") as f:
        rom_array = np.fromfile(f, np.dtype('B'))
        print("Reading {} - {} bytes".format(path, rom_array.size))
        return rom_array


def fillBANK(rom_arr, bank_number, name):
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, OUTPUT)
    print("Programming {} ROM... ".format(name))
    bank_offset = bank_number * BANK_SIZE
    num_copies = math.floor(BANK_SIZE / rom_arr.size)
    for copy_num in range(num_copies):
        print("\tCopy #{} from address {} to {}".format(copy_num + 1, hex(bank_offset + copy_num * rom_arr.size), hex(bank_offset + copy_num * rom_arr.size + rom_arr.size - 1)))
        for address in range(rom_arr.size):
            address_offset = bank_offset + address + copy_num * rom_arr.size
            #writeEEPROM(address_offset, rom_arr[address])
            writeAM29F040(address_offset, rom_arr[address])


# EEPROM layout:
# bank 4:   16 copies of zx80
# bank 3:    8 copies of zx81
# bank 2:    4 copies of 48k
# bank 1:    2 copies of 128k
# bank 0:    2 copies of 128k+2


def setup():

    # Read ROMS and assign bank number:
    rom128kplus2_arr = (readROM("./ROM/128k+2.rom"), 0)
    rom128k_arr = (readROM("./ROM/128k.rom"), 1)
    rom48k_arr = (readROM("./ROM/48k.rom"), 2)
    romzx81_arr = (readROM("./ROM/zx81.rom"), 3)
    romzx80_arr = (readROM("./ROM/zx80.rom"), 4)
    testrom_arr = (readROM("./ROM/testrom.bin"), 5)

    wiringpi.wiringPiSetup()
    wiringpi.pinMode(SHIFT_DATA, OUTPUT)
    wiringpi.pinMode(SHIFT_CLK, OUTPUT)
    wiringpi.pinMode(SHIFT_LATCH, OUTPUT)

    wiringpi.pinMode(WRITE_EN, OUTPUT)
    wiringpi.digitalWrite(WRITE_EN, HIGH) # Disabled

    # Erase entire EEPROM:
    print("Erasing {} bytes...".format(EEPROM_BYTES), end='')
    #for address in range(EEPROM_BYTES):
    #    writeEEPROM(address, 0xff)
    eraseAM29F040()
    time.sleep(15)
    print(" - done")

    # Write a single bank (64k):
    fillBANK(*rom128kplus2_arr, "128K +2")
    fillBANK(*rom128k_arr, "128K")
    fillBANK(*rom48k_arr, "48K")
    fillBANK(*romzx81_arr, "ZX81")
    fillBANK(*romzx80_arr, "ZX80")
    fillBANK(*testrom_arr, "Testrom")

    print("Writing EEPROM DUMP for checking...")
    writeEEPROMFile("./EEPROM_DUMP.bin")
    print("Finished!")


if __name__ == "__main__":
    setup()
