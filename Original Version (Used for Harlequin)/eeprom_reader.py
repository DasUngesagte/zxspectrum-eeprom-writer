# Based partly on Ben Eaters https://github.com/beneater/eeprom-programmer/blob/master/eeprom-programmer/eeprom-programmer.ino

import wiringpi
import time
import numpy as np
import math

SHIFT_DATA = 0          # SER   14
SHIFT_CLK = 1           # SRCLK 11
SHIFT_LATCH = 2         # RCLK  12
WRITE_EN = 4

EEPROM_DATA = [29, 28, 27, 25, 24, 23, 22, 21]

INPUT = 0
OUTPUT = 1
MSBFIRST = 1
LOW = 0
HIGH = 1

EEPROM_BYTES = 524288
BANK_SIZE = 65536


def shiftOut(shift_data, shift_clk, data):
    for i in range(7,-1,-1):
        wiringpi.digitalWrite(shift_data, HIGH if data & (1 << i) != 0 else LOW)
        wiringpi.digitalWrite(shift_clk, HIGH)
        wiringpi.digitalWrite(shift_clk, LOW)


def setAddress(address, outputEnable):

    # set pin 20 HIGH if outputEnable = false
    shiftOut(SHIFT_DATA, SHIFT_CLK, ((address >> 16) | (0x00 if outputEnable else 0x08)))
    shiftOut(SHIFT_DATA, SHIFT_CLK, (address >> 8))
    shiftOut(SHIFT_DATA, SHIFT_CLK, (address))

    wiringpi.digitalWrite(SHIFT_LATCH, LOW)
    wiringpi.digitalWrite(SHIFT_LATCH, HIGH)
    wiringpi.digitalWrite(SHIFT_LATCH, LOW)


def readEEPROM(address):
    setAddress(address, True)
    data = 0
    for pin in reversed(EEPROM_DATA):
        data = (data << 1) + wiringpi.digitalRead(pin)
    return data


def writeEEPROMFile(path):
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, INPUT)
    data_arr = [0xff] * EEPROM_BYTES
    for i in range(EEPROM_BYTES):
        data_arr[i] = readEEPROM(i)

    byte_arr = bytearray(data_arr)
    with open(path, "wb") as f:
        f.write(byte_arr)


def readROM(path):
    with open(path,"rb") as f:
        rom_array = np.fromfile(f, np.dtype('B'))
        print("Reading {} - {} bytes".format(path, rom_array.size))
        return rom_array


# EEPROM layout:
# bank 4:   16 copies of zx80
# bank 3:    8 copies of zx81
# bank 2:    4 copies of 48k
# bank 1:    2 copies of 128k
# bank 0:    2 copies of 128k+2


def setup():

    wiringpi.wiringPiSetup()
    wiringpi.pinMode(SHIFT_DATA, OUTPUT)
    wiringpi.pinMode(SHIFT_CLK, OUTPUT)
    wiringpi.pinMode(SHIFT_LATCH, OUTPUT)
    
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    wiringpi.pinMode(WRITE_EN, OUTPUT)

    print("Writing EEPROM DUMP for checking...")
    writeEEPROMFile("./EEPROM_DUMP.bin")
    print("Finished!")


if __name__ == "__main__":
    setup()






