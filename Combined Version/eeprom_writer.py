#!/usr/bin/python3 -u
# Based partly on Ben Eaters https://github.com/beneater/eeprom-programmer/blob/master/eeprom-programmer/eeprom-programmer.ino

import wiringpi
import time
import numpy as np
import math
import argparse

SHIFT_DATA = 0          # SER   14
SHIFT_CLK = 1           # SRCLK 11
SHIFT_LATCH = 2         # RCLK  12
WRITE_EN = 4

EEPROM_DATA = [29, 28, 27, 25, 24, 23, 22, 21]

INPUT = 0
OUTPUT = 1
MSBFIRST = 1
LOW = 0
HIGH = 1

EEPROM_BYTES = 0    # argparsed
BANK_SIZE = 0       # argparsed

COMMAND_TIME = 0.0001 # 100 µs should suffice, page 29 datasheet


def shiftOut(shift_data, shift_clk, data):
    for i in range(7,-1,-1):
        wiringpi.digitalWrite(shift_data, HIGH if data & (1 << i) != 0 else LOW)
        wiringpi.digitalWrite(shift_clk, HIGH)
        wiringpi.digitalWrite(shift_clk, LOW)


def setAddress(address, outputEnable, atmel):

    # set pin 20 HIGH if outputEnable = false
    if(atmel):
        shiftOut(SHIFT_DATA, SHIFT_CLK, ((address >> 8)   | (0x00 if outputEnable else 0x80)))
    else:
        shiftOut(SHIFT_DATA, SHIFT_CLK, ((address >> 16) | (0x00 if outputEnable else 0x08)))
        shiftOut(SHIFT_DATA, SHIFT_CLK, (address >> 8))

    shiftOut(SHIFT_DATA, SHIFT_CLK, (address))

    wiringpi.digitalWrite(SHIFT_LATCH, LOW)
    wiringpi.digitalWrite(SHIFT_LATCH, HIGH)
    wiringpi.digitalWrite(SHIFT_LATCH, LOW)


def setData(data):
    for pin in EEPROM_DATA:
        wiringpi.digitalWrite(pin, int(data & 1))
        data = data >> 1


def readEEPROM(address, atmel):
    setAddress(address, True, atmel)
    data = 0
    for pin in reversed(EEPROM_DATA):
        data = (data << 1) + wiringpi.digitalRead(pin)
    return data


def writeAM29F040(address, data):
    setAddress(0x555, False, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False, False)
    setData(0xA0)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(address, False, False)
    setData(data)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)


def eraseAM29F040():
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, OUTPUT)

    setAddress(0x555, False, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False, False)
    setData(0x80)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False, False)
    setData(0xAA)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x2AA, False, False)
    setData(0x55)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)
        
    setAddress(0x555, False, False)
    setData(0x10)

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(COMMAND_TIME)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(COMMAND_TIME)


def write28CXXXB(address, data):
    
    setAddress(address, False, True)
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, OUTPUT)
    for pin in EEPROM_DATA:
        wiringpi.digitalWrite(pin, int(data & 1))
        data = data >> 1

    wiringpi.digitalWrite(WRITE_EN, LOW)
    time.sleep(0.00001)
    wiringpi.digitalWrite(WRITE_EN, HIGH)
    time.sleep(0.01)


def writeEEPROMFile(path, atmel):
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, INPUT)
    data_arr = [0xff] * EEPROM_BYTES
    for i in range(EEPROM_BYTES):
        data_arr[i] = readEEPROM(i, atmel)
    byte_arr = bytearray(data_arr)
    with open(path, "wb") as f:
        f.write(byte_arr)


def readROM(path):
    with open(path,"rb") as f:
        rom_array = np.fromfile(f, np.dtype('B'))
        print(f"Reading {path} - {rom_array.size} bytes")
        return rom_array


def fillBANK(rom_arr, bank_number, name, assume_atmel):
    for pin in EEPROM_DATA:
        wiringpi.pinMode(pin, OUTPUT)
    print(f"Programming {name} ROM... ")
    bank_offset = bank_number * BANK_SIZE
    num_copies = math.floor(BANK_SIZE / rom_arr.size)
    for copy_num in range(num_copies):
        print(f"\tCopy #{copy_num + 1} from address {hex(bank_offset + copy_num * rom_arr.size)} to {hex(bank_offset + copy_num * rom_arr.size + rom_arr.size - 1)}")
        for address in range(rom_arr.size):
            address_offset = bank_offset + address + copy_num * rom_arr.size
            if(assume_atmel):
                write28CXXXB(address_offset, rom_arr[address])
            else:
                writeAM29F040(address_offset, rom_arr[address])


def setup():

    global EEPROM_BYTES, BANK_SIZE

    # Arguments:
    ap = argparse.ArgumentParser()
    ap.add_argument("--read", "-r", help="path to the dumped ROM", type=str)
    ap.add_argument("--write", "-w", help="path to the ROM(s) to write", type=str, nargs="+")
    ap.add_argument("--chipsize", "-s", help="EEPROM size in bytes", type=int, default=65536)
    ap.add_argument("--bank", "-b", help="Bank size in bytes", type=int, default=65536)
    ap.add_argument("--atmel", "-a", help="Assume Atmel AT28C***B EEPROMS", action="store_true", default=False)
    ap.add_argument("--skip", "-k", help="Skip erase", action="store_true", default=False)
    args = ap.parse_args()

    EEPROM_BYTES = args.chipsize
    BANK_SIZE = args.bank

    print(f"EEPROM size: {args.chipsize} bytes")
    print(f"Bank size: {args.bank} bytes")

    wiringpi.wiringPiSetup()
    wiringpi.pinMode(SHIFT_DATA, OUTPUT)
    wiringpi.pinMode(SHIFT_CLK, OUTPUT)
    wiringpi.pinMode(SHIFT_LATCH, OUTPUT)
    wiringpi.pinMode(WRITE_EN, OUTPUT)
    wiringpi.digitalWrite(WRITE_EN, HIGH) # Disabled

    if(args.read):
        print(f"Writing EEPROM dump to {args.read}")
        writeEEPROMFile(args.read, args.atmel)
        print("Finished!")

    if(args.write):
        if(not args.skip):
            # Erase entire EEPROM:
            print(f"Erasing {EEPROM_BYTES} bytes...", end='')
            if(args.atmel):
                for address in range(EEPROM_BYTES):
                    write28CXXXB(address, 0x00)
            else:
                eraseAM29F040()
                time.sleep(15)

        print(" - done")

        current_bank = 0
        for rom in args.write:
            rom_arr = readROM(rom)
            fillBANK(rom_arr, current_bank, f"{current_bank + 1}.", args.atmel)
            current_bank += 1

        print("Writing EEPROM DUMP for checking...")
        writeEEPROMFile("./EEPROM_DUMP.bin", args.atmel)
        print("Finished!")


if __name__ == "__main__":
    setup()
